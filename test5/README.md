# 实验5：包，过程，函数的用法

## 实验目的

- 了解`PL/SQL`语言结构

- 了解`PL/SQL`变量和常量的声明和使用方法

- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。

2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。

3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。 Oracle递归查询的语句格式是：

   ~~~sql
   SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
   START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
   CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
   ~~~

## 实验步奏

1. **以hr用户登录，创建MyPack包**

   ~~~sql
   create or replace PACKAGE MyPack IS
   FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
   PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
   END MyPack;
   /
   ~~~

   ![](./createpack.png)

2. **创建有关函数和过程**

   ~~~sql
   create or replace PACKAGE BODY MyPack IS
   FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
   AS
       N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
       BEGIN
       SELECT SUM(salary) into N  FROM EMPLOYEES E
       WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
       RETURN N;
       END;
   
   PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
   AS
       LEFTSPACE VARCHAR(2000);
       begin
       --通过LEVEL判断递归的级别
       LEFTSPACE:=' ';
       --使用游标
       for v in
           (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
           START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
           CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
       LOOP
           DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                               V.EMPLOYEE_ID||' '||v.FIRST_NAME);
       END LOOP;
       END;
   END MyPack;
   /
   ~~~

   ![](./createfunction.jpg)

3. 函数`Get_SalaryAmount()`测试方法：

   ~~~sql
   select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
   ~~~

   ![](./testgetsalary.jpg)

4. **测试过程Get_Employees()**

   ~~~sql
   set serveroutput on
   DECLARE
   V_EMPLOYEE_ID NUMBER;    
   BEGIN
   V_EMPLOYEE_ID := 101;
   MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
   END;
   /
   ~~~

   ![](./testprodure.jpg)

   ## 实验总结

   通过本次实验我了解到`PL/SQL`是**Oracle**数据库中的过程式编程语言，它允许将逻辑控制结构和`SQL`语句嵌套在一起。通过学习，我了解到`PL/SQL`代码通常由块组成，每个块包含三部分：声明部分、可执行部分和异常处理部分。这种结构使得代码更易于阅读和维护。

   在学习包、过程和函数方面，我认识到它们在组织和管理代码方面具有重要作用。包是一种封装相关对象（如过程、函数、类型等）的方式；而过程和函数则是用于执行特定任务的可重用程序单元。过程没有返回值，而函数则有一个明确的返回值。通过编写实例代码，我了解到如何创建和调用包、过程和函数，以及如何在不同场景下选择使用它们。
