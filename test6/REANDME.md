## 实验6 基于Oracle数据库的商品销售系统的设计

设计一套基于Oracle数据库的商品销售系统的数据库设计方案。

- 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
- 设计权限及用户分配方案。至少两个用户。
- 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
- 设计一套数据库的备份方案。



### 用户设计

#### 创建用户

为了有足够的权限能够进行表空间设计和数据表创建，需要创建一个属于这个系统的管理员用户用来进行增删改查的相关操作，以及一个普通用户，只能处理一部分数据，且对其他的系统设置无法做出更改。

**admin**

~~~sql
CREATE USER admin_sale IDENTIFIED BY 123456;
GRANT CONNECT, RESOURCE, DBA TO admin_sale;
~~~

![](./admin_create.jpg)

**user**

~~~sql
CREATE USER user_sale IDENTIFIED BY 123456;
GRANT CONNECT, RESOURCE TO user_sale;
~~~

![](./user_create.jpg)

#### admin身份连接sqldeveloper

![](./sqldeveloper.jpg)

### 创建表空间

#### 创建订单详情表空间

~~~sql
CREATE TABLESPACE tbs_order_detail_index
DATAFILE '/home/oracle/database/tbs_order_detail_index'
SIZE 100M AUTOEXTEND ON NEXT 50M MAXSIZE UNLIMITED;
~~~

![](./tbs_order_detail.jpg)

#### 创建商品信息表空间

~~~sql
CREATE TABLESPACE tbs_order_detail_index
DATAFILE '/home/oracle/database/tbs_product_index'
SIZE 100M AUTOEXTEND ON NEXT 50M MAXSIZE UNLIMITED;
~~~

![](./tbs_product_index.jpg)

#### 创建订单的表空间

~~~sql
CREATE TABLESPACE tbs_orders_index
DATAFILE '/home/oracle/database/tbs_orders_index'
SIZE 100M AUTOEXTEND ON NEXT 50M MAXSIZE UNLIMITED;
~~~

![](./tbs_orders_index.jpg)

#### 创建用户表空间

~~~sql
CREATE TABLESPACE tbs_user_index
DATAFILE '/home/oracle/database/tbs_user_index'
SIZE 100M AUTOEXTEND ON NEXT 50M MAXSIZE UNLIMITED;
~~~

![](./tbs_user_index.jpg)

#### 表空间描述

每个表空间设置初始大小为100M，允许自动拓展，每次自动拓展增加50M的大小，且最大尺寸不限制。



### 创建表

#### 商品表设计

商品表中需要有**产品id**作为主键，**产品名称**、**价格**、**种类**、**供应商**等信息。

~~~sql
CREATE TABLE products (
product_id NUMBER(10) PRIMARY KEY,
product_name VARCHAR2(50),
price NUMBER(10,2),
category VARCHAR2(50),
supplier VARCHAR2(50)
) TABLESPACE tbs_product_index;
~~~

![](./create_products.jpg)

#### 订单表设计

订单包含**订单id**作为主键，**客户id**作为外键，**订单创建日期**，**总价格**等字段。

~~~sql
CREATE TABLE orders (
order_id NUMBER(10) PRIMARY KEY,
customer_id NUMBER(10),
order_date DATE,
total_price NUMBER(10,2)
) TABLESPACE tbs_orders_index;
~~~

![](./create_orders.jpg)

#### 订单详情表设计

订单详情表首先需要**订单id**标记自己所属的订单，之后定义**商品id**，**数量**，**价格**等信息。

~~~sql
CREATE TABLE order_details (
order_id NUMBER(10),
product_id NUMBER(10),
quantity NUMBER(10),
price NUMBER(10,2),
CONSTRAINT order_details_primary_key PRIMARY KEY (order_id, product_id)
) TABLESPACE tbs_order_detail_index;
~~~

![](./create_order_detail.jpg)

#### 用户表设计

用户表用来保存用户信息，包括**姓名**、**邮箱**、**电话号码**、**住址**等信息。

~~~sql
CREATE TABLE customers (
customer_id NUMBER(10) PRIMARY KEY,
customer_name VARCHAR2(50),
email VARCHAR2(50),
phone_number VARCHAR2(20),
address VARCHAR2(100)
) TABLESPACE tbs_user_index;
~~~

![](./create_user.jpg)

#### 库存表设计

库存表**存储商品的id**表示哪个商品，以及该商品的**库存数量**。

~~~sql
CREATE TABLE inventory (
product_id NUMBER(10) PRIMARY KEY,
stock_quantity NUMBER(10)
) TABLESPACE tbs_product_index;
~~~

![](./create_stock.jpg)



### 输入模拟数据

#### 商品表

~~~sql
DECLARE
    TYPE string_list IS VARRAY(20) OF VARCHAR2(50);
    products string_list := string_list(
        'Product1', 'Product2', 'Product3', 'Product4', 'Product5',
        'Product6', 'Product7', 'Product8', 'Product9', 'Product10',
        'Product11', 'Product12', 'Product13', 'Product14', 'Product15',
        'Product16', 'Product17', 'Product18', 'Product19', 'Product20'
    );
    categories string_list := string_list(
        'Category1', 'Category2', 'Category3', 'Category4', 'Category5'
    );

BEGIN
    FOR i IN 1 .. 100000 LOOP
        INSERT INTO products (
            product_id, product_name, price, category, supplier
        ) VALUES (
            i,
            products(TRUNC(DBMS_RANDOM.VALUE(1, 20))),
            DBMS_RANDOM.VALUE(0.99, 49.99),
            categories(TRUNC(DBMS_RANDOM.VALUE(1, 5))),
            'ACME Inc'
        );
        
        IF MOD(i, 1000) = 0 THEN
            COMMIT;
        END IF;
    END LOOP;
END;
~~~

![](./data_products.jpg)

![](D:\Oracle\oracle\test6\show_data_products.jpg)

#### 订单表

~~~sql
INSERT INTO orders(order_id, customer_id, order_date, total_price)
SELECT level, mod(level, 10000)+1, DATE '2023-01-01' + mod(level, 2500), mod(level, 1000)+1
FROM dual
CONNECT BY level <= 100000;
~~~

![](./data_orders.jpg)

#### 订单详情表

~~~sql
INSERT INTO order_details (order_id, product_id, quantity, price)
SELECT level, mod(level, 1000)+1, mod(level, 10)+1, mod(level, 100)+1
FROM dual
CONNECT BY level <= 100000;
~~~

![](./data_order_details.jpg)

#### 库存表

~~~sql
DECLARE
min_product_id NUMBER := 1;
max_product_id NUMBER := 100000;
BEGIN
FOR i IN min_product_id .. max_product_id LOOP
INSERT INTO inventory (product_id, stock_quantity)
VALUES (i, TRUNC(DBMS_RANDOM.VALUE(1, 100)));
    IF MOD(i, 1000) = 0 THEN
        COMMIT;
    END IF;
END LOOP;
END;
~~~

![](./data_stock.jpg)

### 程序包设计

创建一个程序包命名为：`sales_manage`：

1. `update_order`：更新订单信息以及对应产品的库存信息；
2. `cancel_order`：取消订单信息；
3. `find_order_details_by_id`：根据订单ID查看订单详情；

~~~sql
CREATE OR REPLACE PACKAGE sales_manage AS
    -- 存储过程1：将客户订单保存到订单表中，并更新库存表中的库存数量
    PROCEDURE update_order(
        p_product_id IN NUMBER,
        p_quantity IN NUMBER,
        p_customer_id IN NUMBER
    );

    -- 存储过程2：取消客户订单，释放库存数量并删除订单表中的订单
    PROCEDURE cancel_order(
        p_order_id IN NUMBER
    );

    -- 函数3：根据订单ID查看订单详情
    FUNCTION find_order_details_by_id(
        p_order_id IN NUMBER 
    ) RETURN SYS_REFCURSOR;
END sales_manage;
/
~~~

![](./package.jpg)

~~~sql
create or replace PACKAGE BODY sales_manage AS
    -- 存储过程1：将客户订单保存到订单表中，并更新库存表中的库存数量
    PROCEDURE update_order(
        p_product_id IN NUMBER,
        p_quantity IN NUMBER,
        p_customer_id IN NUMBER
    ) IS
        v_order_id NUMBER;
        v_current_stock NUMBER;
    BEGIN
        -- 生成订单ID
        SELECT order_sequence.NEXTVAL INTO v_order_id FROM dual;

        -- 插入订单数据到orders表
        INSERT INTO orders (order_id, customer_id, order_date)
        VALUES (v_order_id, p_customer_id, SYSDATE);

        -- 获取当前库存数量
        SELECT stock_quantity INTO v_current_stock FROM inventory WHERE product_id = p_product_id;

        -- 检查库存是否足够
        IF v_current_stock >= p_quantity THEN
            -- 更新订单详情表
            INSERT INTO order_details (order_id, product_id, quantity, price)
            VALUES (v_order_id, p_product_id, p_quantity, (SELECT price FROM products WHERE product_id = p_product_id));

            -- 更新库存表中的库存数量
            UPDATE inventory
            SET stock_quantity = v_current_stock - p_quantity
            WHERE product_id = p_product_id;
            -- 提交事务
            COMMIT;
        ELSE
            NULL;
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
            -- 处理异常
            ROLLBACK;
            RAISE;
    END update_order;

    -- 存储过程2：取消客户订单，释放库存数量并删除订单表中的订单
    PROCEDURE cancel_order(
        p_order_id IN NUMBER
    ) IS
        v_product_id NUMBER;
        v_quantity NUMBER;
    BEGIN
        -- 获取订单详情中的产品ID和数量
        SELECT product_id, quantity INTO v_product_id, v_quantity
        FROM order_details
        WHERE order_id = p_order_id;

        -- 更新库存表中的库存数量
        UPDATE inventory SET stock_quantity = stock_quantity + v_quantity WHERE product_id = v_product_id;

        -- 删除订单表中的订单
        DELETE FROM orders WHERE order_id = p_order_id;

        -- 提交事务
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            -- 处理异常
            ROLLBACK;
            RAISE;
    END cancel_order;

    -- 函数3：根据订单ID查看订单详情
    FUNCTION find_order_details_by_id(
        p_order_id IN NUMBER 
    ) RETURN SYS_REFCURSOR IS
        v_cursor SYS_REFCURSOR;
    BEGIN
        -- 查询订单详情
        OPEN v_cursor FOR
        SELECT o.order_id, o.order_date, od.product_id, p.product_name, od.quantity, od.price
        FROM orders o
        JOIN order_details od ON o.order_id = od.order_id
        JOIN products p ON od.product_id = p.product_id
        WHERE o.order_id = p_order_id;

        -- 返回游标
        RETURN v_cursor;
    EXCEPTION
        WHEN OTHERS THEN
            RAISE;
    END find_order_details_by_id;
END sales_manage;
~~~

![](./package_info.jpg)

**一些测试**

~~~sql
BEGIN
  sales_manage.update_order(p_product_id => 1001, p_quantity => 2, p_customer_id => 10001);
END;
~~~

![](./test1.jpg)

![](./test1_1.jpg)

### 备份方案

使用`Oracle`自带的备份工具`RMAN`来创建备份：

首先登录到根数据库下，以`sys`的身份

~~~sql
sqlplus / as sysdba
~~~

关闭数据库并以MOUNT模式重新启动数据库

~~~sql
SQL> SHUTDOWN IMMEDIATE
Database closed.
Database dismounted.
ORACLE instance shut down.
SQL> STARTUP MOUNT
ORACLE instance started.
Total System Global Area 1191178968 bytes
Fixed Size		    9134808 bytes
Variable Size		  352321536 bytes
Database Buffers	  822083584 bytes
Redo Buffers		    7639040 bytes
Database mounted.
~~~

![](./shut_mount.jpg)

~~~sql
SQL> ALTER DATABASE ARCHIVELOG;

Database altered.
~~~

![](./archivelog.jpg)

~~~sql
[oracle@localhost ~]$ rman target /

Recovery Manager: Release 19.0.0.0.0 - Production on Fri May 26 00:21:34 2023
Version 19.3.0.0.0

Copyright (c) 1982, 2019, Oracle and/or its affiliates.  All rights reserved.

connected to target database: ORCL (DBID=1658190882, not open)

RMAN>  SHOW ALL;

using target database control file instead of recovery catalog
RMAN configuration parameters for database with db_unique_name ORCL are:
CONFIGURE RETENTION POLICY TO REDUNDANCY 1; # default
CONFIGURE BACKUP OPTIMIZATION OFF; # default
CONFIGURE DEFAULT DEVICE TYPE TO DISK; # default
CONFIGURE CONTROLFILE AUTOBACKUP ON; # default
CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '%F'; # default
CONFIGURE DEVICE TYPE DISK PARALLELISM 1 BACKUP TYPE TO BACKUPSET; # default
CONFIGURE DATAFILE BACKUP COPIES FOR DEVICE TYPE DISK TO 1; # default
CONFIGURE ARCHIVELOG BACKUP COPIES FOR DEVICE TYPE DISK TO 1; # default
CONFIGURE MAXSETSIZE TO UNLIMITED; # default
CONFIGURE ENCRYPTION FOR DATABASE OFF; # default
CONFIGURE ENCRYPTION ALGORITHM 'AES128'; # default
CONFIGURE COMPRESSION ALGORITHM 'BASIC' AS OF RELEASE 'DEFAULT' OPTIMIZE FOR LOAD TRUE ; # default
CONFIGURE RMAN OUTPUT TO KEEP FOR 7 DAYS; # default
CONFIGURE ARCHIVELOG DELETION POLICY TO NONE; # default
CONFIGURE SNAPSHOT CONTROLFILE NAME TO '/u01/app/oracle/product/19.0.0/dbhome_1/dbs/snapcf_orcl19c.f'; # default

RMAN> BACKUP DATABASE;

Starting backup at 26-MAY-23
allocated channel: ORA_DISK_1
channel ORA_DISK_1: SID=622 device type=DISK
channel ORA_DISK_1: starting full datafile backup set
channel ORA_DISK_1: specifying datafile(s) in backup set
input datafile file number=00010 name=/u01/app/oracle/oradata/ORCL/pdb/undotbs01.dbf
input datafile file number=00011 name=/u01/app/oracle/oradata/ORCL/pdb/users01.dbf
input datafile file number=00008 name=/u01/app/oracle/oradata/ORCL/pdb/system01.dbf
input datafile file number=00009 name=/u01/app/oracle/oradata/ORCL/pdb/sysaux01.dbf
input datafile file number=00012 name=/home/oracle/database/tbs_order_detail_index
input datafile file number=00013 name=/home/oracle/database/tbs_product_index
input datafile file number=00014 name=/home/oracle/database/tbs_orders_index
input datafile file number=00015 name=/home/oracle/database/tbs_user_index
channel ORA_DISK_1: starting piece 1 at 26-MAY-23
channel ORA_DISK_1: finished piece 1 at 26-MAY-23
piece handle=/u01/app/oracle/product/19.0.0/dbhome_1/dbs/021t2vm1_1_1 tag=TAG20230526T002225 comment=NONE
channel ORA_DISK_1: backup set complete, elapsed time: 00:00:07
channel ORA_DISK_1: starting full datafile backup set
channel ORA_DISK_1: specifying datafile(s) in backup set
input datafile file number=00001 name=/u01/app/oracle/oradata/ORCL/system01.dbf
input datafile file number=00003 name=/u01/app/oracle/oradata/ORCL/sysaux01.dbf
input datafile file number=00005 name=/u01/app/oracle/oradata/ORCL/undotbs01.dbf
input datafile file number=00007 name=/u01/app/oracle/oradata/ORCL/users01.dbf
channel ORA_DISK_1: starting piece 1 at 26-MAY-23
channel ORA_DISK_1: finished piece 1 at 26-MAY-23
piece handle=/u01/app/oracle/product/19.0.0/dbhome_1/dbs/031t2vm9_1_1 tag=TAG20230526T002225 comment=NONE
channel ORA_DISK_1: backup set complete, elapsed time: 00:00:03
channel ORA_DISK_1: starting full datafile backup set
channel ORA_DISK_1: specifying datafile(s) in backup set
input datafile file number=00006 name=/u01/app/oracle/oradata/ORCL/pdbseed/undotbs01.dbf
input datafile file number=00002 name=/u01/app/oracle/oradata/ORCL/pdbseed/system01.dbf
input datafile file number=00004 name=/u01/app/oracle/oradata/ORCL/pdbseed/sysaux01.dbf
channel ORA_DISK_1: starting piece 1 at 26-MAY-23
channel ORA_DISK_1: finished piece 1 at 26-MAY-23
piece handle=/u01/app/oracle/product/19.0.0/dbhome_1/dbs/041t2vmc_1_1 tag=TAG20230526T002225 comment=NONE
channel ORA_DISK_1: backup set complete, elapsed time: 00:00:01
Finished backup at 26-MAY-23

Starting Control File Autobackup at 26-MAY-23
piece handle=/u01/app/oracle/product/19.0.0/dbhome_1/dbs/c-1658190882-20230526-00 comment=NONE
Finished Control File Autobackup at 26-MAY-23

RMAN> LIST BACKUP;


List of Backup Sets
===================


BS Key  Type LV Size       Device Type Elapsed Time Completion Time
------- ---- -- ---------- ----------- ------------ ---------------
1       Full    17.95M     DISK        00:00:00     25-MAY-23      
        BP Key: 1   Status: AVAILABLE  Compressed: NO  Tag: TAG20230525T220035
        Piece Name: /u01/app/oracle/product/19.0.0/dbhome_1/dbs/arch/ORCL/autobackup/2023_05_25/o1_mf_n_1137794435_l6ytj3m7_.bkp
  Control File Included: Ckp SCN: 3302228      Ckp time: 25-MAY-23

BS Key  Type LV Size       Device Type Elapsed Time Completion Time
------- ---- -- ---------- ----------- ------------ ---------------
2       Full    1.52G      DISK        00:00:05     26-MAY-23      
        BP Key: 2   Status: AVAILABLE  Compressed: NO  Tag: TAG20230526T002225
        Piece Name: /u01/app/oracle/product/19.0.0/dbhome_1/dbs/021t2vm1_1_1
  List of Datafiles in backup set 2
  Container ID: 3, PDB Name: PDB
  File LV Type Ckp SCN    Ckp Time  Abs Fuz SCN Sparse Name
  ---- -- ---- ---------- --------- ----------- ------ ----
  8       Full 3318425    26-MAY-23              NO    /u01/app/oracle/oradata/ORCL/pdb/system01.dbf
  9       Full 3318425    26-MAY-23              NO    /u01/app/oracle/oradata/ORCL/pdb/sysaux01.dbf
  10      Full 3318425    26-MAY-23              NO    /u01/app/oracle/oradata/ORCL/pdb/undotbs01.dbf
  11      Full 3318425    26-MAY-23              NO    /u01/app/oracle/oradata/ORCL/pdb/users01.dbf
  12      Full 3318425    26-MAY-23              NO    /home/oracle/database/tbs_order_detail_index
  13      Full 3318425    26-MAY-23              NO    /home/oracle/database/tbs_product_index
  14      Full 3318425    26-MAY-23              NO    /home/oracle/database/tbs_orders_index
  15      Full 3318425    26-MAY-23              NO    /home/oracle/database/tbs_user_index

BS Key  Type LV Size       Device Type Elapsed Time Completion Time
------- ---- -- ---------- ----------- ------------ ---------------
3       Full    597.73M    DISK        00:00:01     26-MAY-23      
        BP Key: 3   Status: AVAILABLE  Compressed: NO  Tag: TAG20230526T002225
        Piece Name: /u01/app/oracle/product/19.0.0/dbhome_1/dbs/031t2vm9_1_1
  List of Datafiles in backup set 3
  File LV Type Ckp SCN    Ckp Time  Abs Fuz SCN Sparse Name
  ---- -- ---- ---------- --------- ----------- ------ ----
  1       Full 3318656    26-MAY-23              NO    /u01/app/oracle/oradata/ORCL/system01.dbf
  3       Full 3318656    26-MAY-23              NO    /u01/app/oracle/oradata/ORCL/sysaux01.dbf
  5       Full 3318656    26-MAY-23              NO    /u01/app/oracle/oradata/ORCL/undotbs01.dbf
  7       Full 3318656    26-MAY-23              NO    /u01/app/oracle/oradata/ORCL/users01.dbf

BS Key  Type LV Size       Device Type Elapsed Time Completion Time
------- ---- -- ---------- ----------- ------------ ---------------
4       Full    471.38M    DISK        00:00:01     26-MAY-23      
        BP Key: 4   Status: AVAILABLE  Compressed: NO  Tag: TAG20230526T002225
        Piece Name: /u01/app/oracle/product/19.0.0/dbhome_1/dbs/041t2vmc_1_1
  List of Datafiles in backup set 4
  Container ID: 2, PDB Name: PDB$SEED
  File LV Type Ckp SCN    Ckp Time  Abs Fuz SCN Sparse Name
  ---- -- ---- ---------- --------- ----------- ------ ----
  2       Full 1134255    02-MAR-23              NO    /u01/app/oracle/oradata/ORCL/pdbseed/system01.dbf
  4       Full 1134255    02-MAR-23              NO    /u01/app/oracle/oradata/ORCL/pdbseed/sysaux01.dbf
  6       Full 1134255    02-MAR-23              NO    /u01/app/oracle/oradata/ORCL/pdbseed/undotbs01.dbf

BS Key  Type LV Size       Device Type Elapsed Time Completion Time
------- ---- -- ---------- ----------- ------------ ---------------
5       Full    17.95M     DISK        00:00:00     26-MAY-23      
        BP Key: 5   Status: AVAILABLE  Compressed: NO  Tag: TAG20230526T002237
        Piece Name: /u01/app/oracle/product/19.0.0/dbhome_1/dbs/c-1658190882-20230526-00
  Control File Included: Ckp SCN: 3318656      Ckp time: 26-MAY-23

RMAN> 
~~~

![](./rman.jpg)

![](./backup_all.jpg)

这样就是实现了数据库的备份工作。

1. 创建备份存储目录

首先，您需要选择一个适当的目录来存储PDB备份文件。我们将使用目录路径 `/backup`。

```sql
CREATE OR REPLACE DIRECTORY backup_dir AS '/backup';
```

​	2.定期完整备份

定期执行完整备份以备份整个PDB的数据和结构。

```sql
  -- 生成备份文件名
  DECLARE
    v_backup_file VARCHAR2(100);
  BEGIN
    v_backup_file := 'full_backup_' || TO_CHAR(SYSDATE, 'YYYYMMDD_HH24MISS') || '.dbf';

    -- 执行完整备份
    EXECUTE IMMEDIATE 'ALTER PLUGGABLE DATABASE BACKUP';
    
    -- 移动备份文件到指定目录
    EXECUTE IMMEDIATE 'ALTER PLUGGABLE DATABASE BEGIN BACKUP';
    EXECUTE IMMEDIATE 'ALTER PLUGGABLE DATABASE END BACKUP';
    EXECUTE IMMEDIATE 'ALTER PLUGGABLE DATABASE MOVE DATAFILE ALL TO ''' || 'backup_dir' || '/' || v_backup_file || '''';
  END;
  
  COMMIT;
END;
/
```

上述代码将生成一个备份文件名，并执行完整备份操作。备份文件将被存储在指定的备份目录中

​	3.增量备份

增量备份用于在完整备份之后备份发生更改的数据。

```sql
  -- 生成备份文件名
  DECLARE
    v_backup_file VARCHAR2(100);
  BEGIN
    v_backup_file := 'incremental_backup_' || TO_CHAR(SYSDATE, 'YYYYMMDD_HH24MISS') || '.dbf';

    -- 执行增量备份
    EXECUTE IMMEDIATE 'ALTER PLUGGABLE DATABASE BEGIN INCREMENTAL BACKUP';
    EXECUTE IMMEDIATE 'ALTER PLUGGABLE DATABASE END INCREMENTAL BACKUP';
    EXECUTE IMMEDIATE 'ALTER PLUGGABLE DATABASE MOVE DATAFILE ALL TO ''' || 'backup_dir' || '/' || v_backup_file || '''';
  END;
  
  COMMIT;
END;
/
```

上述代码将生成一个备份文件名，并执行增量备份操作。备份文件将被存储在指定的备份目录中。

建议是每天执行一次增量备份，每周执行一次完整备份。另外，为了保证备份的完整性和可靠性，建议将备份文件保存到不同的存储介质或云存储服务中，以防止单点故障和数据丢失。此外，还可以考虑定期测试备份文件的可恢复性，以确保备份方案的有效性。

### 总结

在本次实验中，设计了一个商品销售的数据库系统，并实现了一些存储过程和函数来处理复杂的业务逻辑。以下是对本次实验的总结：

1. 数据库设计：
   - 创建了四个主要的表：`products`、`orders`、`order_details`和`customers`，分别用于存储商品信息、订单信息、订单详情和客户信息。
   - 每个表都有适当的字段和约束，以确保数据的完整性和一致性。
   - 通过定义主键、外键和索引，提高了数据库的性能和查询效率。
2. 存储过程和函数：
   - 创建了一个名为 `sales_manage` 的程序包，其中包含了三个存储过程和一个函数。
   - `update_order` 存储过程用于将客户订单保存到订单表中，并更新库存表中的库存数量。
   - `cancel_order` 存储过程用于取消客户订单，释放库存数量并删除订单表中的订单。
   - `find_order_details_by_id` 函数根据订单ID查找订单详情，并返回一个游标结果集。
3. 数据库备份方案：
   - 完成了一个简单的备份方案示例，包括定期完整备份和增量备份。
   - 通过创建备份存储目录，并编写相应的备份代码，实现了数据备份和恢复的功能。
   - 备份文件可以存储在指定的目录中，以确保数据的安全性和可靠性。

通过本次实验，我深入理解了商品销售数据库系统的设计和实现。学习了如何创建表、定义约束、编写存储过程和函数，以及实现简单的备份方案。这些知识和技能对于开发和维护实际的数据库应用程序非常有价值。

总而言之，本次实验为我提供了宝贵的实践经验，使我能够更好地理解和应用数据库设计和管理的知识。通过设计和实现商品销售数据库系统，我掌握了数据库建模、存储过程和函数的使用，以及简单备份方案的编写。想必这些知识都会成为之后开发路上不可或缺的一环。
